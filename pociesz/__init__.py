import json
from pathlib import Path

from redbot.core.bot import Red

from .pociesz import Pociesz


async def setup(bot: Red) -> None:
    bot.add_cog(Pociesz(bot))
