from typing import Literal, Dict

import discord
from discord.utils import get
from redbot.core import commands
from redbot.core.bot import Red
from redbot.core.config import Config

RequestType = Literal["discord_deleted_user", "owner", "user", "user_strict"]


class Starynajebany(commands.Cog):
    """
    Urozmaicanie dyskusji niedetermenizmem
    """

    def __init__(self, bot: Red, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.bot = bot
        self.config = Config.get_conf(
            self,
            identifier=124656332126965342,
            force_registration=True,
        )

    async def red_delete_data_for_user(self, *, requester: RequestType, user_id: int) -> None:
        # TODO: Replace this with the proper end user data removal handling.
        await super().red_delete_data_for_user(requester=requester, user_id=user_id)

    async def dej_emoji(self, id: str, guild) -> object:
        return get(await guild.fetch_emojis(), name=id)

    @commands.Cog.listener()
    async def on_message(self, message: discord.Message):
        if message.author.id == self.bot.user.id:
            return

        szypul = await self.dej_emoji('szypul', message.guild)
        bdsm = await self.dej_emoji('BDSM', message.guild)
        doliwa = await self.dej_emoji('i_see_u', message.guild)
        ropiak = await self.dej_emoji('najwiekszy_python_w_miescie', message.guild)

        kto = {
            ('studentka', 'warunek', 'knn'): szypul,
            ('doliwa', 'warunek', 'dyskret'): doliwa,
            ('python',): ropiak,
        }

        lower = message.content.lower()

        if 'szypul' in lower:
            await message.channel.send("skurwysyn")
        elif 'chuj' in lower:
            await message.channel.send("Czy chodziło ci o **Jacek Szypulski**" + str(bdsm) + "?")
        for k, v in kto.items():
            if any(query in lower for query in k):
                await message.add_reaction(v)
        # if 'studentka' in message.content:
        #     await message.add_reaction(szypul)
        # if 'warunek' in message.content:
        #     await message.add_reaction(doliwa)
        # if 'python' in message.content:
        #     await message.add_reaction(ropiak)
