import json
from pathlib import Path

from redbot.core.bot import Red

from .Watch import Watch


async def setup(bot: Red) -> None:
    bot.add_cog(Watch(bot))
