from typing import Literal

import discord
from redbot.core import commands
from redbot.core.bot import Red
from redbot.core.config import Config

RequestType = Literal["discord_deleted_user", "owner", "user", "user_strict"]


class Pociesz(commands.Cog):
    """
    A short description of the cog.
    """

    def __init__(self, bot: Red) -> None:
        self.bot = bot
        
    @commands.command()
    async def pociesz(self, ctx):
        """This does stuff!"""
        # Your code will go here
        await ctx.send("Wiesz że szanse na to że Twoim promotorem zostanie szypul są niezerowe?")

    async def red_delete_data_for_user(self, *, requester: RequestType, user_id: int) -> None:
        # TODO: Replace this with the proper end user data removal handling.
        super().red_delete_data_for_user(requester=requester, user_id=user_id)
