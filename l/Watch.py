import asyncio
import random
from typing import Literal

import discord
from redbot.core import commands
from discord.utils import get
from redbot.core.bot import Red
from redbot.core.config import Config
from redbot.core.data_manager import cog_data_path

from github import Github


class Watch(commands.Cog):
    """
    A short description of the cog.
    """

    def __init__(self, bot: Red, *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.bot = bot
    
    @commands.command()
    async def papaj(self, ctx: commands.Context):
        response = None
        async with ctx.typing():
            github_keys = await self.bot.get_shared_api_tokens("github")
            token = github_keys.get("token")
            if  token is None:
                await ctx.send("Brakuje tokena do GH :(")
                return
        
            try:
                g = Github(token)
                repo = g.get_repo("cenzopapa/img")
                contents = repo.get_contents("")
                el = random.choice(contents)
                response = el.download_url
            except Exception as e:
                await ctx.send("Spierdoliło się :-(")
                await ctx.bot.send_to_owners(e)
        
        await ctx.send(response)
